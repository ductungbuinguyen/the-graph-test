import { DogMinted, Transfer } from '../generated/Contract/Contract';
import { TokenInformation } from '../generated/schema';

export function handleDogMinted(event: DogMinted): void {
	let tokenInformation = TokenInformation.load(event.params.tokenId.toString());
	if (!tokenInformation) {
		tokenInformation = new TokenInformation(event.params.tokenId.toString());
	}
	tokenInformation.owner = event.transaction.from
	tokenInformation.mintedTimestamp = event.block.timestamp
	tokenInformation.save();
}

export function handleTransfer(event: Transfer): void {
	let tokenInformation = TokenInformation.load(event.params.tokenId.toString());
	if (tokenInformation) {
		tokenInformation.owner = event.params.to
		tokenInformation.save();
	}
}